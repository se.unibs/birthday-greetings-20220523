package it.unibs.se.birthday;

import java.util.List;

public interface EmployeeProvider {
    List<Employee> getAllEmployees();
}
