package it.unibs.se.birthday;

import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) throws URISyntaxException {
        var clock = new SystemLocalClock();
        var provider = new FileSystemBasedEmployeeProvider(Path.of(Monolith.class.getResource("/employees.txt").toURI()));
        PrintWriter writer = new PrintWriter(System.out);
        var notifier = new PrintWriterGreetingsNotifier(writer);
        var service = new GreetingsService(provider, notifier, clock);
        service.execute();
        writer.flush();
    }
}

class SystemLocalClock implements LocalClock {
    @Override
    public LocalDate today() {
        return LocalDate.now();
    }
}
