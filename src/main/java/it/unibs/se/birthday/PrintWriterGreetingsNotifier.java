package it.unibs.se.birthday;

import java.io.PrintWriter;

public class PrintWriterGreetingsNotifier implements GreetingsNotifier {
    private final PrintWriter writer;

    public PrintWriterGreetingsNotifier(PrintWriter writer) {
        this.writer = writer;
    }

    public void notifyBirthdayGreetingsTo(Employee employee) {
        writer.println(String.format("Happy birthday, %s (to be sent to %s)", employee.getFirstName(), employee.getEmail()));
    }
}
