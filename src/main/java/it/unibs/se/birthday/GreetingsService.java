package it.unibs.se.birthday;

public class GreetingsService {
    private final EmployeeProvider provider;
    private final GreetingsNotifier notifier;
    private final LocalClock clock;

    public GreetingsService(EmployeeProvider provider, GreetingsNotifier notifier, LocalClock clock) {
        this.provider = provider;
        this.notifier = notifier;
        this.clock = clock;
    }

    public void execute() {
        var today = clock.today();
        for (var employee: provider.getAllEmployees()) {
            if(employee.getBirthDay().getDayOfMonth() == today.getDayOfMonth() && employee.getBirthDay().getMonth() == today.getMonth()) {
                notifier.notifyBirthdayGreetingsTo(employee);
            }
        }
    }
}
