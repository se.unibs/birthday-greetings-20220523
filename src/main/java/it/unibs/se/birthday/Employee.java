package it.unibs.se.birthday;

import java.time.LocalDate;

public class Employee {
    private final String firstName;
    private final LocalDate birthDay;
    private final String email;

    public Employee(String firstName, String lastName, LocalDate birthDay, String email) {
        this.firstName = firstName;
        this.birthDay = birthDay;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public String getFirstName() {
        return firstName;
    }
}
