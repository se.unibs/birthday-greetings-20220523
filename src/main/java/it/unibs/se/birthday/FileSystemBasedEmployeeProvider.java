package it.unibs.se.birthday;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class FileSystemBasedEmployeeProvider implements EmployeeProvider {
    private final Path file;

    public FileSystemBasedEmployeeProvider(Path file) {
        this.file = file;
    }

    public List<Employee> getAllEmployees() {
        var employees = new LinkedList<Employee>();
        try {
            var rows = Files.readAllLines(file);
            for(var i = 0; i < rows.size(); i++) {
                if(i != 0) {
                    var fields = rows.get(i).split(",");
                    var firstName = fields[1];
                    var lastName = fields[0];
                    var birthDay = LocalDate.parse(fields[2].trim());
                    var email = fields[3].trim();
                    employees.add(new Employee(firstName, lastName, birthDay, email));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return employees;
    }
}
