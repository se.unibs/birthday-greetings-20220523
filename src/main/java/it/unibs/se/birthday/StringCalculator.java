package it.unibs.se.birthday;

//import java.util.Arrays;
//import java.util.stream.Collectors;

public class StringCalculator {
    private final String delimiter;

    public StringCalculator(String delimiter) {
        this.delimiter = delimiter;
    }

    public StringCalculator() {
        this(",");
    }

    public int handle(String input) {
        if (input == "") {
            return 0;
        }
//        return Arrays.stream(input
//                .split(","))
//                .sequential()
//                .map(Integer::parseInt)
//                .reduce((x, y) -> x + y)
//                .get()
//                ;
        var items = input.split(delimiter);
        var sum = 0;
        for (var item : items) {
            sum += Integer.parseInt(item);
        }
        return sum;
    }
}
