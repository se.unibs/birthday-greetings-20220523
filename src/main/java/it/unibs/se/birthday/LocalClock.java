package it.unibs.se.birthday;

import java.time.LocalDate;

public interface LocalClock {
    LocalDate today();
}
