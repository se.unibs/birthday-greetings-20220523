package it.unibs.se.birthday;

public interface GreetingsNotifier {
    void notifyBirthdayGreetingsTo(Employee employee);
}
