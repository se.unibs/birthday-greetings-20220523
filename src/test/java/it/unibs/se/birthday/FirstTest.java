package it.unibs.se.birthday;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class FirstTest {
    @Test
    public void firstTest() {
        assertThat(1, is(equalTo(1)));
    }
}
