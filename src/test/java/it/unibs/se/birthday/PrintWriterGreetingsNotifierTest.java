package it.unibs.se.birthday;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class PrintWriterGreetingsNotifierTest {
    @Test
    public void shouldWriteBirthdayGreetings() {
        var employee = new Employee("Pietro", "Martinelli", LocalDate.of(1978, 3, 19), "pietrom@example.com");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        var writer = new PrintWriter(out);
        var notifier = new PrintWriterGreetingsNotifier(writer);
        notifier.notifyBirthdayGreetingsTo(employee);
        writer.flush();
        var output = out.toString();
        assertThat(output, containsString("Happy birthday, Pietro (to be sent to pietrom@example.com)"));
    }
}
