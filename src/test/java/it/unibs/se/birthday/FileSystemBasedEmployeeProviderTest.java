package it.unibs.se.birthday;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

public class FileSystemBasedEmployeeProviderTest {
    @Test
    public void shouldReadEmptyListFromEmptyFile() throws IOException {
        var tempFile = Files.createTempFile("test-employee", ".txt");
        Files.write(tempFile, Arrays.asList("last_name, first_name, date_of_birth, email"));
        var provider = new FileSystemBasedEmployeeProvider(tempFile);
        assertThat(provider.getAllEmployees().size(), is(equalTo(0)));
    }

    @Test
    public void shouldReadAllEmployeesData() throws IOException {
        var tempFile = Files.createTempFile("test-employee", ".txt");
        Files.write(tempFile, Arrays.asList(
                "last_name, first_name, date_of_birth, email",
                "Martinelli, Pietro, 1978-03-19, pietrom@qualcosa.it",
                "Verdi, Giuseppe, 1813-10-10, peppev@qualcosa.it"
        ));
        var provider = new FileSystemBasedEmployeeProvider(tempFile);
        assertThat(provider.getAllEmployees().size(), is(equalTo(2)));
    }

    @Test
    public void shouldReadEmailAddresses() throws IOException {
        var tempFile = Files.createTempFile("test-employee", ".txt");
        Files.write(tempFile, Arrays.asList(
                "last_name, first_name, date_of_birth, email",
                "Martinelli, Pietro, 1978-03-19, pietrom@qualcosa.it",
                "Verdi, Giuseppe, 1813-10-10, peppev@qualcosa.it"
        ));
        var provider = new FileSystemBasedEmployeeProvider(tempFile);
        List<String> emails = provider.getAllEmployees()
                .stream()
                .map(x -> x.getEmail())
                .collect(Collectors.toList());
        assertTrue(emails.contains("pietrom@qualcosa.it"));
    }
}
