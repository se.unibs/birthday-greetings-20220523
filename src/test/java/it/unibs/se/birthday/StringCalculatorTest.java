package it.unibs.se.birthday;

import org.junit.Test;

import static com.spun.util.Asserts.assertEqual;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/*
"" --> 0
"19" --> 19
"19,11,17" --> 47
"19;11;22" --> 52
* */
public class StringCalculatorTest {
    @Test
    public void handleEmptyInput() {
        var calculator = new StringCalculator();
        var result = calculator.handle("");
        assertThat(result, is(equalTo(0)));
    }

    @Test
    public void handleInputContainingSingleNumber() {
        var calculator = new StringCalculator();
        var n = 19;
        var result = calculator.handle(Integer.toString(n));
        assertThat(result, is(equalTo(n)));
    }

    @Test
    public void shouldSumNumbersInInput() {
        var calculator = new StringCalculator();
        var result = calculator.handle("19,11,17");
        assertThat(result, is(equalTo(47)));
    }

    @Test
    public void canSpecifyCustomDelimiter() {
        var calculator = new StringCalculator(";");
        var result = calculator.handle("19;11;22");
        assertThat(result, is(equalTo(52)));
    }
}
