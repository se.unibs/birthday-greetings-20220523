package it.unibs.se.birthday;

import org.junit.Test;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GreetingsServiceTest {
    private static final LocalDate ADate = LocalDate.of(1978, 3, 19);
    @Test
    public void test() {
        var employees = new LinkedList<Employee>();
        employees.add(new Employee("Pietro", "Martinelli", ADate, "pm@example.com"));
        employees.add(new Employee("Pedro", "Martinelli", LocalDate.of(2000, 3, 20), "pm2@example.com"));
        EmployeeProvider provider = new InMemoryEmployeeProvider(employees);
        GreetingsNotifier notifier = new InMemoryGreetingsNotifier();
        LocalClock clock = new FixedLocalClock(LocalDate.of(2022, 3, 19));
        var service = new GreetingsService(provider, notifier, clock);
        service.execute();

        assertThat(((InMemoryGreetingsNotifier)notifier).getEmployeesToBeNotified().get(0).getEmail(), is(equalTo("pm@example.com")));
        assertThat(((InMemoryGreetingsNotifier)notifier).getEmployeesToBeNotified().size(), is(equalTo(1)));
    }
}

class InMemoryEmployeeProvider implements EmployeeProvider {
    private final List<Employee> employees;

    public InMemoryEmployeeProvider(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employees;
    }
}

class InMemoryGreetingsNotifier implements GreetingsNotifier {
    private final List<Employee> employeesToBeNotified = new LinkedList<>();

    public List<Employee> getEmployeesToBeNotified() {
        return employeesToBeNotified;
    }

    @Override
    public void notifyBirthdayGreetingsTo(Employee employee) {
        employeesToBeNotified.add(employee);
    }
}

class FixedLocalClock implements LocalClock {
    private final LocalDate aDate;

    public FixedLocalClock(LocalDate aDate) {

        this.aDate = aDate;
    }

    @Override
    public LocalDate today() {
        return aDate;
    }
}